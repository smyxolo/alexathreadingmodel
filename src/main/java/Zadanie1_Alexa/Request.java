package Zadanie1_Alexa;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Request {
    private String request;
    private AlexaDevice deviceToCall;

    public Request(String request, AlexaDevice deviceToCall) {
        this.request = request;
        this.deviceToCall = deviceToCall;
    }
}
