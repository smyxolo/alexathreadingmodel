package Zadanie1_Alexa.skills;

import java.time.LocalDateTime;

public class DateTimeSill extends AbstractSkill {
    @Override
    public void run() {
        System.out.println("ALEXA: the time is: " + LocalDateTime.now());
    }
}
