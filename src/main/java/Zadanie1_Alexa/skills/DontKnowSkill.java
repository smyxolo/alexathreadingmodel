package Zadanie1_Alexa.skills;

public class DontKnowSkill extends AbstractSkill {
    @Override
    public void run() {
        System.out.println("ALEXA: Command not recognised.");
    }
}
