package Zadanie1_Alexa;

import Zadanie1_Alexa.skills.AbstractSkill;

import java.util.Observable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AlexaDevice extends Observable{
    private static int alexaIds = 0;
    private int id = alexaIds;
    private ExecutorService es = Executors.newSingleThreadExecutor();

    public AlexaDevice() {
        this.id = alexaIds++;
    }

    public void invoke(AbstractSkill skill){
        es.submit(skill);
    }

    public void sendRequest(String requestString){
        if (requestString.toLowerCase().startsWith("alexa")) {
            setChanged();
            notifyObservers(new Request(requestString, this));
        }
    }
}
