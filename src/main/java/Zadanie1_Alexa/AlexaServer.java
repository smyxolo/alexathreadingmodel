package Zadanie1_Alexa;

import Zadanie1_Alexa.skills.AbstractSkill;
import Zadanie1_Alexa.skills.DateTimeSill;
import Zadanie1_Alexa.skills.DontKnowSkill;

import java.util.Observable;
import java.util.Observer;

public class AlexaServer implements Observer{

    public AlexaServer() {
    }

    public AbstractSkill parseRequest(String request){
        if(request.toLowerCase().contains("time")){
            return new DateTimeSill();
        }
        else return new DontKnowSkill();
    }

    @Override
    public void update(Observable o, Object arg) {
        AbstractSkill callback = null;
        if(arg instanceof Request){
            callback = parseRequest(((Request) arg).getRequest());
            ((Request) arg).getDeviceToCall().invoke(callback);
        }
    }
}
