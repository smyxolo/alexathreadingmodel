package Zadanie1_Alexa;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        AlexaDevice device = new AlexaDevice();
        AlexaServer as = new AlexaServer();

        Scanner scan = new Scanner(System.in);

        while (true){
            String inputLine = scan.nextLine();
            switch (inputLine){
                case "register" : {
                    if (device.countObservers() == 0){
                        device.addObserver(as);
                        System.out.println("Connected.");
                        break;
                    }
                    else{
                        System.out.println("You are already connected.");
                        break;
                    }

                }

                case "unregister" : {
                    device.deleteObserver(as);
                    System.out.println("Disconnected.");
                    break;
                }
                default: {
                    device.sendRequest(inputLine);
                    break;
                }
            }
        }
    }
}
