Alexa Processor.
Stwórzmy symulator systemu Alexa App.
Schemat na rysunku (tablica) dla nieobecnych wrzucę na slack'a.
Piszemy aplikację która będzie symulować działanie Alexy. Alexa jest urządzeniem które umieszczamy w domu. Ponieważ jest to maluśkie urządzenie, nie posiada mocy obliczeniowej do wykonywania działań. Alexa przetwarza dźwięk. Przynajmniej tak wygląda. W rzeczywistości łączy się z chmurą która jest dużej mocy obliczeniowej i tam wykonuje się całe rozpoznawanie mowy. Spróbujemy zrobić aplikację która działa tak samo i posiada element wielowątkowy. Ponieważ chcemy mieć możliwość wykonywania więcej niż jednej komendy na aleksie, musimy sprawić by działania które wykonuje, były wykonywane asynchronicznie ( w nowym wątku ).
Zadania:
1.) Stwórz klasę AlexaDevice która posiada pusty konstruktor, oraz pole id (które się generuje [licznik]).
1a) Wskazówka:
    private static int alexaIds = 0;
    private int id = alexaIds;
    public AlexaDevice() {
    }
2.) Stwórz metodę sendRequest której zadaniem będzie "wykonanie" komendy przez urządzenie. W rzeczywistości będziemy tą komendę wysyłać na serwer. Parametrem metody jest komenda "mówiona" (w rzeczywistości pisana w main'ie).
2a) Wskazówka:

    public void sendRequest(String requestString) {
    }
3.) Stwórz klasę Request która reprezentuje zapytanie wysyłane z AlexaDevice i będzie wysyłana do serwera. Jest to klasa modelu która posiada pola: String request, oraz urządzenie wywołujące. Stwórz konstruktor oraz gettery i settery.
3a) Wskazówka:
public class Request {
    private String request;
    private AlexaDevice deviceToCall;
    public Request(String request, AlexaDevice deviceToCall) {
        this.request = request;
        this.deviceToCall = deviceToCall;
    }
    public String getRequest() {
        return request;
    }
    public void setRequest(String request) {
        this.request = request;
    }
    public AlexaDevice getDeviceToCall() {
        return deviceToCall;
    }
    public void setDeviceToCall(AlexaDevice deviceToCall) {
        this.deviceToCall = deviceToCall;
    }
}
4.) Stwórz klasę AlexaServer która reprezentuje serwer do którego łączy się urządzenie (AlexaDevice). Stwórz serwer jako singleton lub jako obiekt Observer (implements Observer). Stwórz w niej metodę update (jeśli zrobiłeś/łaś klasę jako observer) lub stwórz metodę update która posiada dwa parametry. Pierwszym ma być klasa wywołująca (AlexaDevice) drugą ma być Request.
4a) Wskazówka:
public class AlexaServer implements Observer {
    public AlexaServer() {
    }
    public void update(Observable o, Object arg) {
    }
}
4b) Wskazówka:
public class alexa.AlexaServer {
    public final static alexa.AlexaServer instance = new alexa.AlexaServer();
    private alexa.AlexaServer(){};
}
5.) W klasie AlexaDevice, w metodzie sendRequest wywołaj metodę update w klasie AlexaServer i przekaż jej obiekt typu Request. Obiekt Request powinien być stworzony na początku w/w metody i być wysyłany mechanizmem observer/observable lub wywołanie metody update.
5a) Wskazówka:
            Request request = new Request(requestString, this);
            // 1)
            // informujemy serwer o nowym requeście
            alexa.AlexaServer.instance.parseRequest(request);
            // 2)
            setChanged();
            notifyObservers(request);
6.) Dodaj weryfikację w metodzie sendRequest. Alexa (urządzenie) reaguje tylko na komendy które rozpoczynają się od słowa "alexa". Jeśli komenda nie zaczyna się od słowa alexa, to nie powinno się wykonywać przekazanie komendy do AlexaServer.
6a) Wskazówka:
    if (requestString.toLowerCase().startsWith("alexa ")) {
            Request request = new Request(requestString, this);
            // 1)
            // informujemy serwer o nowym requeście
            //alexa.AlexaServer.instance.parseRequest(request);
            // 2)
            setChanged();
            notifyObservers(request);
        }
7.) Stwórz klasę AbstractSkill która reprezentuje umiejętność Alexy. AbstractSkill powinna być Runnable i klasą abstrakcyjną.
7a) Wskazówka:
public abstract class AbstractSkill implements Runnable {
}
8.) Stwórz klasę dziedziczącą po AbstractSkill, o nazwie DateTimeSkill która powinna w metodzie run wypisywać aktualny czas.
8a) Wskazówka:
public class DateTimeSkill extends AbstractSkill {
    public DateTimeSkill() {
    }
    public void run() {
        System.out.println("ALEXA: the time is " + LocalDateTime.now());
    }
}
9.) Stwórz klasę DontKnowSkill która również dziedziczy po tej samej klasie i w metodzie run mówi użytkownikowi że alexa nie zna takiej komendy. Ten skill będzie wywoływany jeśli alexa nie rozpozna komendy.
10.) W klasie AlexaServer stwórz metodę parseRequest która przyjmuje String request z zapytania, które AlexaDevice wywołało w metodzie update. Wywołaj metodę parseRequest. W metodzie parseRequest stwórz implementację która rozpoznaje komendy. Zmodyfikuj metodę tak aby zwracała obiekt typu AbstractSkill(sygnatura metody). W metodzie update, wywołanie parseRequest (teraz zwraca obiekt) powinno przypisywać wynik (obiekt wynikowy) do zmiennej.
Jeśli komenda rozpoczyna się od słów:
alexa what's the time
lub
alexa what time is it
powinna zwrócić nowy obiekt typu DateTimeSkill
Jeśli komenda rozpoczyna się od innych słów, zwróć obiekt klasy DontKnowSkill.
Wskazówka 1)
    public AbstractSkill parseRequest(String req) {

    }
Wskazówka 2)
        req = req.toLowerCase();
        if (req.startsWith("alexa")) {
            req = req.substring(6); // 5 znaków oraz
            if (req.toLowerCase().startsWith("what's the time") ||
                    req.toLowerCase().startsWith("give me the time") ||
                    req.toLowerCase().startsWith("the time") ||
            req.toLowerCase().startsWith("what time is it")) {
                return new DateTimeSkill();
            }
        }
        return new DontKnowSkill();
Wskazówka 3)
    AbstractSkill callback = parseRequest(request);
11.) W klasie AlexaDevice stwórz metodę invoke która jako parametr przyjmuje obiekt typu AbstractSkill. Po wywołaniu przekaż ten obiekt do wykonania dla wątku. (Trzeba stworzyć obiekt wątku jako pole (pula
wątków) lub lokalnie nowy wątek.
Wskazówka 1)
        executorService.submit(callback);
Wskazówka 2)
    private ExecutorService executorService = Executors.newSingleThreadExecutor();
12.) W klasie AlexaServer po wywołaniu parseRequest w metodzie update otrzymaliśmy obiekt AbstractSkill. Z obiektu Request możemy wyciągnąć obiekt AlexaDevice. W tym obiekcie wywołaj nową metodę i przekaż do niej otrzymany z metody parseRequest.
Wskazówka 1)
            request.getDeviceToCall().invoke(callback);


13.) Jeśli stworzyłeś/łaś klasę AlexaDevice jako Observer/Observable, to obiekty klasy AlexaDevice powinny dodawać AlexaServer jako Observera. Stwórz maina w której dzieje się właśnie to^. Stwórz dwa obiekty i dodaj server jako observera device. Następnie stwórz Scanner. Implementacja Scannera podana jest niżej:
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (line.equalsIgnoreCase("register")) { // podłącz internet
                if (device.countObservers() == 0) {
                    device.addObserver(server);
                }
            } else if (line.equalsIgnoreCase("unregister")) { // rozłącz internet
                device.deleteObserver(server);
            } else {
                device.sendRequest(line); // wykonaj zapytanie do alexy
            }
        }
14.) Przetestuj aplikację. Sprawdź czy zwraca czas.
13.**) Dodaj skille do Alexy. Dodamy umiejętność przechowywania zadań TODO. Dodaj 3 skille:
0. SetTimerSkill - uruchamia timer, który po ukończeniu pracy ma wypisać komunikat.
1. AddTodoSkill
2. RemoveTodoSkill
3. ListTodoSkills
Zaimplementuj je. (implementacja klasy, magazynu(opcjonalne), oraz parsowania)
https://bitbucket.org/nordeagda2/alexasimulator