package Zadanie2_SKM_INPROGRESS;

import lombok.Getter;

import java.time.LocalDateTime;

@Getter

public class TrainInfo implements Comparable {
    private String trainID;
    private LocalDateTime arrivalTime;

    @Override
    public int compareTo(Object o) {
        if (this.arrivalTime.isAfter(((TrainInfo) o).arrivalTime)) return 1;
        else return -1;
    }
}
