Stworzymy aplikację symulującą pracę linii kolejowych na pewnym odcinku.
Celem stworzenia aplikacji jest przypomnienie sobie o wzorcach projektowych, oraz ćwiczenia związane z pracą aplikacji na wielu wątkach.
Opis systemu:
Istnieje linia kolejowa (np. SKM) łącząca ze sobą bardzo duży obszar. Zakładamy, że jesteśmy firmą informatyzującą ten obszar transportu. Istnieją linie kolejowe oraz pociagi, a także stacje kolejowe na trasie (powiedzmy Gdańsk - Wejherowo). Pociągi poruszają się zgodnie z rozkładem ale nie istnieje żaden system monitorowania pracy kolei. Stacje nie są ze sobą połączone. Tworzymy system który pozwoli nam spiąć całość systemu w jedno. Do tego stworzymy modele stacji i naszym założeniem jest, że istnienie instancji klasy w projekcie oznacza istnienie rzeczywistego obiektu. Wywołanie na obiekcie metody oznacza przesłanie do obiektu pewnej informacji.
Elementy systemu:
Station - stacja kolejowa.
Train - pociąg.
Headquarters - centrala zarządcza monitorująca pociągi.
Informacje o wszystkich obiektach znajdują się w centrali (headquarters). Informatyzujemy cały system. Celem ćwiczenia jest zaprojektowanie logiki która pozwoli monitorować poruszanie się pociągów na wybranej trasie. Nie tworzymy tutaj grafików, ale próbujemy stworzyć system który będzie informować stacje (Station) o zbliżających się pociągach tak, aby na stacjach mogły się wyświetlić informacje o tym kiedy zbliżają się 3 najbliższe pociągi.
Pociągi są obiektami wykonywalnymi (Runnable). Możemy wystartować pociąg ze stacji początkowej lub stacji końcowej (dla uproszczenia tylko z tych stacji). Pociąg posiada rozkład (TrainSchedule) na którym posiada nazwy stacji oraz (tylko) czas jaki musi jechać aby dotrzeć do tej stacji z ostatniej stacji na której był (pociąg posiada tylko różnice czasów między stacjami). Za każdym razem gdy pociąg dojeżdza do stacji informuje (wykonamy to implementując wzorzec projektowy observer observable) headquarters (centralę) o tym, że dojechał do stacji (informując go tylko o swoim id oraz nazwie stacji do której dojechał). Proces poruszania się pociągu (odczekiwania) może zostać wykonany za pomocą timerów lub runnable.
Centrala (headquarters) przekazuje informacje. Powiadomienia z pociągów traktujemy jak system śledzenia gps pociągów. Po tym jak pociąg poinformuje nas(headquarters) o dotarciu na stację, rozsyłamy (jak?) informacje do wszystkich stacji o tym, że pociąg dotarł do podanej stacji. Identyfikatory pociągów powinny być unikalne, w ten sposób unikniemy pomyłek. Centrala rozsyła informacje o dotarciu do wszystkich stacji, a stacje odbieraja te informacje i obliczają czas jaki pociąg potrzebuje do dotarcia do TEJ stacji. Np. możemy to zrobić na podstawie grafiku - Stacje po otrzymaniu informacji o zbliżającym się pociągu obliczają na podstawie grafiku pociągu czas, w jakim pociąg pojawi się na tej stacji.
Headquarters - klasa która posiada pola:
lista stacji
lista grafików wszystkich pociągów które wyjeżdzają ze stacji początkowej.
lista grafików wszystkich pociągów które wyjeżdzają ze stacji końcowej.
lista czasów dzielących wszystkie stacje. (jeśli stacji jest 8, to czasów powinno być 7 - zrozumiałe ?)
Każda stacja ma listę minimum 3 obiektów które reprezentują pociągi jadące
w jedną stronę, oraz 3 obiekty reprezentujące pociągi jadące w 2 strone.
#####!!!!!!!!!!! Zanim rozpoczniesz zadanie daj znać, muszę wysłać repozytorium z zadaniem !!!!!###########