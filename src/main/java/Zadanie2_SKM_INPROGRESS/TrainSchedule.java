package Zadanie2_SKM_INPROGRESS;

import lombok.Getter;

import java.time.Duration;
import java.util.*;

@Getter

public class TrainSchedule {

    Map<Station, Duration> schedule = new HashMap<>();
    List<Station> route = new LinkedList<>();

}
